//
//  AnswersHelper.swift
//  Iu
//
//  Created by Gilson Gil on 5/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Crashlytics

struct AnswersHelper {
  static func signIn(success: Bool) {
    #if RELEASE
      Answers.logLoginWithMethod(nil, success: success, customAttributes: nil)
    #endif
  }
  
  static func signOut() {
    #if RELEASE
      Answers.logCustomEventWithName("Sign out", customAttributes: nil)
    #endif
  }
  
  static func invite(media: String?) {
    #if RELEASE
      var attributes: [String: AnyObject] = ["Invited": true]
      if let media = media {
        attributes["Media"] = media
      }
      Answers.logInviteWithMethod(nil, customAttributes: attributes)
    #endif
  }
  
  static func pageView(pageName: String) {
    #if RELEASE
      Answers.logContentViewWithName(pageName, contentType: "View", contentId: nil, customAttributes: nil)
    #endif
  }
}

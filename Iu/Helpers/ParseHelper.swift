//
//  ParseHelper.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse
import Locksmith

enum ParseError: ErrorType {
  case Unhandled
  case NotFound
  case Network
  
  func description() -> String {
    switch self {
    case .Unhandled:
      return "Erro desconhecido"
    case .NotFound:
      return Strings.LoginNotFoundError
    case .Network:
      return Strings.LoginNetworkError
    }
  }
}

struct ParseHelper {
  static let iuAccountString = "iu."
  
  static func register() {
    guard let dict = NSBundle.mainBundle().infoDictionary?["Parse"] as? [String: String], applicationId = dict["ApplicationId"], clientKey = dict["ClientKey"] else {
      return
    }
    Parse.enableLocalDatastore()
    Parse.setApplicationId(applicationId, clientKey: clientKey)
  }
  
  static func isLoggedIn(completion: Bool -> ()) {
    pinnedPhone {
      if let _ = $0 {
        completion(true)
      } else {
        completion(false)
      }
    }
  }
  
  static func pinnedPhone(completion: Int? -> ()) {
    guard let phone = Locksmith.loadDataForUserAccount("iu.")?["phone"] as? Int else {
      completion(nil)
      return
    }
    completion(phone)
  }
  
  static func searchPhone(phone: Double, completion: (inner: () throws -> [Professional]) -> ()) {
    guard Reachability.isConnectedToNetwork() else {
      completion { throw ParseError.Network }
      return
    }
    PFCloud.callFunctionInBackground("student_get_teachers", withParameters: ["phone_number": phone]) { data, error in
      let success = error == nil
      AnswersHelper.signIn(success)
      guard success else {
        completion { throw ParseError.Unhandled }
        return
      }
      guard let data = data as? [AnyObject] where data.count > 0 else {
        completion { throw ParseError.NotFound }
        return
      }
      let professionals: [Professional]
      if let json = data as? [[String: AnyObject]] {
        professionals = json.flatMap {
          Professional(json: $0)
        }
      } else if let users = data as? [PFObject] {
        professionals = users.flatMap {
          Professional(name: $0["name"] as? String, objectId: $0.objectId)
        }
      } else {
        completion { throw ParseError.NotFound }
        return
      }
      try! Locksmith.updateData(["phone": phone], forUserAccount: "iu.")
      completion { return professionals }
    }
  }
  
  static func getProfessionalSummary(professionalId: String, completion: (inner: () throws -> [String: AnyObject]) -> ()) {
    guard Reachability.isConnectedToNetwork() else {
      completion { throw ParseError.Network }
      return
    }
    pinnedPhone { phone in
      guard let phone = phone else {
        completion { throw ParseError.Unhandled }
        return
      }
      PFCloud.callFunctionInBackground("student_get_summary", withParameters: ["teacher_id": professionalId, "phone_number": phone]) { data, error in
        let success = error == nil
        guard success else {
          completion { throw ParseError.Unhandled }
          return
        }
        guard let data = data as? [String: AnyObject] else {
          completion { throw ParseError.NotFound }
          return
        }
        completion { return data }
      }
    }
  }
  
  static func getInstructors(completion: (inner: () throws -> [Professional]) -> ()) {
    pinnedPhone {
      guard let phone = $0 else {
        completion { throw ParseError.Unhandled }
        return
      }
      searchPhone(Double(phone), completion: completion)
    }
  }
  
  static func givenClasses(professionalId: String, page: Int, completion: (inner: () throws -> [NSDate]) -> ()) {
    pinnedPhone {
      guard let phone = $0 else {
        completion { throw ParseError.Unhandled }
        return
      }
      
      PFCloud.callFunctionInBackground("classes_get_done", withParameters: ["phone_number": phone, "teacher_id": professionalId, "page": page]) { data, error in
        guard error == nil else {
          completion { throw ParseError.Unhandled }
          return
        }
        guard let data = data as? [PFObject] else {
          completion { throw ParseError.Unhandled }
          return
        }
        let dates = data.flatMap { dataObject -> NSDate? in
          dataObject["usedAt"] as? NSDate
        }
        completion { return dates }
      }
    }
  }
  
  static func signout() {
    do {
      try Locksmith.deleteDataForUserAccount("iu.")
    } catch {
      
    }
    AnswersHelper.signOut()
  }
}

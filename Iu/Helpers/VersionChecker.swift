//
//  VersionChecker.swift
//  Iu
//
//  Created by Gilson Gil on 4/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

struct VersionChecker {
  static func version(completion: (Int, Int)? -> ()) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    let url = NSURL(string: Constants.versionCheckerLink)!
    let dataTask = NSURLSession(configuration: .defaultSessionConfiguration()).dataTaskWithURL(url) { data, response, error in
      dispatch_async(dispatch_get_main_queue()) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      }
      guard let httpResponse = response as? NSHTTPURLResponse where httpResponse.statusCode == 200 else {
        completion(nil)
        return
      }
      guard let data = data, json = try? NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject], result = json?["iu"] as? [String: AnyObject], versionString = result["ios_version"] as? Int, daysToUpdate = result["days_to_update"] as? Int else {
        completion(nil)
        return
      }
      let tuple = (versionString, daysToUpdate)
      completion(tuple)
    }
    dataTask.resume()
  }
  
  static func hasUpdate(completion: (update: Bool, force: Bool) -> ()) {
    version { tuple in
      guard let installedVersion = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String else {
        completion(update: false, force: false)
        return
      }
      completion(update: Int(installedVersion) != tuple?.0, force: tuple?.1 <= 0)
    }
  }
}

//
//  UIImage+iu.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIImage {
  static func iuLogo() -> UIImage? {
    return UIImage(named: "imgLogo")
  }
  
  static func iuOps() -> UIImage? {
    return UIImage(named: "imgOps")
  }
  
  static func iuEnvelop1() -> UIImage? {
    return UIImage(named: "imgEnvelop1")
  }
  
  static func iuEnvelop2() -> UIImage? {
    return UIImage(named: "imgEnvelop2")
  }
  
  static func iuEnvelop3() -> UIImage? {
    return UIImage(named: "imgEnvelop3")
  }
  
  static func iuEnvelop4() -> UIImage? {
    return UIImage(named: "imgEnvelop4")
  }
}

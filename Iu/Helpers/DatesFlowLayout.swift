//
//  DatesFlowLayout.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

enum DatesFlowLayoutStyle {
  case White, Dark
}

final class DatesFlowLayout: UICollectionViewFlowLayout {
  let style: DatesFlowLayoutStyle
  
  required init?(coder aDecoder: NSCoder) {
    style = .Dark
    super.init(coder: aDecoder)
    minimumLineSpacing = 8
    minimumInteritemSpacing = 8
  }
  
  override init() {
    style = .Dark
    super.init()
    minimumLineSpacing = 8
    minimumInteritemSpacing = 8
  }
  
  init(style: DatesFlowLayoutStyle) {
    self.style = style
    super.init()
    minimumLineSpacing = 8
    minimumInteritemSpacing = 8
  }
  
  override func prepareLayout() {
    let separatorClass: AnyClass
    switch style {
    case .White:
      separatorClass = DatesCollectionViewSeparator.classForCoder()
    default:
      separatorClass = DatesCollectionViewDarkSeparator.classForCoder()
    }
    registerClass(separatorClass, forDecorationViewOfKind: "Separator")
    collectionView?.subviews.forEach {
      guard let decorationView = $0 as? DatesCollectionViewSeparator else {
        return
      }
      decorationView.removeFromSuperview()
    }
  }
  
  override func layoutAttributesForDecorationViewOfKind(elementKind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
    guard collectionView?.dataSource?.collectionView(collectionView!, numberOfItemsInSection: 0) > 0 else {
      return super.layoutAttributesForDecorationViewOfKind(elementKind, atIndexPath: indexPath)
    }
    
    let cellAttributes = layoutAttributesForItemAtIndexPath(indexPath)
    let layoutAttributes = UICollectionViewLayoutAttributes(forDecorationViewOfKind: elementKind, withIndexPath: indexPath)
    let baseFrame = cellAttributes!.frame
    
    if elementKind == "Separator" {
      layoutAttributes.frame = CGRect(x: baseFrame.origin.x + baseFrame.width, y: baseFrame.origin.y, width: 8, height: baseFrame.height)
      layoutAttributes.zIndex = -1
      return layoutAttributes
    }
    
    layoutAttributes.zIndex = -1
    return layoutAttributes
  }
  
  override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    let baseLayoutAttributes = super.layoutAttributesForElementsInRect(rect)
    var layoutAttributes = baseLayoutAttributes
    baseLayoutAttributes?.forEach {
      if $0.representedElementCategory == .Cell {
        if !indexPathLastInLine($0.indexPath) {
          if let newLayoutItem = layoutAttributesForDecorationViewOfKind("Separator", atIndexPath: $0.indexPath) {
            layoutAttributes?.append(newLayoutItem)
          }
        }
      }
    }
    return layoutAttributes
  }
  
  func indexPathLastInLine(indexPath: NSIndexPath) -> Bool {
    guard let collectionView = collectionView, let count = collectionView.dataSource?.collectionView(collectionView, numberOfItemsInSection: indexPath.section) where indexPath.item < count else {
      return false
    }
    let lastItem = indexPath.item == count - 1
    let lastInLine = ((indexPath.item + 1) % 3) == 0
    return lastItem || lastInLine
  }
}

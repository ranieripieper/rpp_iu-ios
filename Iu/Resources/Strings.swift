//
//  Strings.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Strings {
  static let LoginTypePhone = NSLocalizedString("LOGIN_TYPEPHONE", comment: "Type phone placeholder")
  static let LoginButton = NSLocalizedString("LOGIN_BUTTON", comment: "Login button label")
  static let LoginNotFoundError = NSLocalizedString("LOGIN_NOTFOUND_ERROR", comment: "Login user not found error message")
  static let LoginNetworkError = NSLocalizedString("LOGIN_NETWORK_ERROR", comment: "Login network error")
  
  static let ClassesAvailableClasses = NSLocalizedString("CLASSES_AVAILABLE_CLASSES", comment: "Available classes label")
  static let ClassesUsedClasses = NSLocalizedString("CLASSES_USED_CLASSES", comment: "Used classes label")
  static let ClassesClass = NSLocalizedString("CLASSES_CLASS", comment: "Class label")
  static let ClassesClasses = NSLocalizedString("CLASSES_CLASSES", comment: "Classes label")
  static let ClassesNoClasses = NSLocalizedString("CLASSES_NOCLASSES", comment: "No classes label")
  static let ClassesPayment = NSLocalizedString("CLASSES_PAYMENT", comment: "Used class payment label")
  static let ClassesPayments = NSLocalizedString("CLASSES_PAYMENTS", comment: "Used classes payment label")
  static let ClassesNoPayment = NSLocalizedString("CLASSES_NOPAYMENT", comment: "No used classes payment label")
  static let ClassesLogout = NSLocalizedString("CLASSES_LOGOUT", comment: "Logout label")
  static let ClassesGiven = NSLocalizedString("CLASSES_CLASSESGIVEN", comment: "Classes given label")
  static let ClassesSeeMore = NSLocalizedString("CLASSES_SEEMORE", comment: "Classes see more button")
  
  static let InviteLabel = NSLocalizedString("INVITE_LABEL", comment: "Invite label text")
  static let InviteButton = NSLocalizedString("INVITE_INVITEBUTTON", comment: "Invite button title")
  static let InviteLaterButton = NSLocalizedString("INVITE_LATER", comment: "Invite later button title")
  static let InviteShare = NSLocalizedString("INVITE_SHARE", comment: "Invite share text")
  
  static let AlertVersionLabel = NSLocalizedString("ALERT_VERSIONLABEL", comment: "Alert version label text")
  static let AlertUpdateButton = NSLocalizedString("ALERT_UPDATEBUTTON", comment: "Alert update button title")
  static let AlertUpdateNowButton = NSLocalizedString("ALERT_UPDATENOWBUTTON", comment: "Alert update now button title")
  static let AlertCancelButton = NSLocalizedString("ALERT_CANCELBUTTON", comment: "Alert cancel button title")
  
  static let ProfessionalsClassesWith = NSLocalizedString("PROFESSIONALS_CLASSESWITH", comment: "Professionals classes with label text")
}

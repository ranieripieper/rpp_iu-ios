//
//  Constants.swift
//  Iu
//
//  Created by Gilson Gil on 2/16/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

struct Constants {
  static let defaultAnimationDuration: NSTimeInterval = 0.5
  static let defaultAnimationDelay: NSTimeInterval = 0
  static let defaultAnimationDamping: CGFloat = 0.8
  static let defaultAnimationInitialVelocity: CGFloat = 1
  static let minimumPhoneLength: Int = 10
  static let maximumPhoneLength: Int = 11
  static let topViewHeight: CGFloat = 82
  
  static let appStoreLink = "https://itunes.apple.com/us/app/iu./id1081967886?ls=1&mt=8"
  static let iuProAppStoreLink = "https://itunes.apple.com/us/app/iu./id1086122688?ls=1&mt=8"
  static let versionCheckerLink = "http://www.iuapp.co/api/v1/config.json"
}

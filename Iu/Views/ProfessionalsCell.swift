//
//  ProfessionalsCell.swift
//  Iu
//
//  Created by Gilson Gil on 4/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ProfessionalsCell: UITableViewCell {
  private let nameLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textColor = UIColor.iuLightGreyColor()
    label.text = Strings.ProfessionalsClassesWith
    label.font = UIFont.iuBlackFont(18)
    addSubview(label)
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.textColor = UIColor.iuUglyPinkColor()
    nameLabel.font = UIFont.iuBlackFont(20)
    addSubview(nameLabel)
    
    let metrics = ["margin": 30]
    let views = ["label": label, "nameLabel": nameLabel]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[label]-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    let horizontalConstraints2 = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[nameLabel]-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints2)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[label][nameLabel]-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
  
  func update(viewModel: ProfessionalsCellViewModel) {
    nameLabel.text = "\(viewModel.prefix)\(viewModel.name)"
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ProfessionalsCell: Updatable {
  typealias ViewModel = ProfessionalsCellViewModel
}

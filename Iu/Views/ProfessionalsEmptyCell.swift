//
//  ProfessionalsEmptyCell.swift
//  Iu
//
//  Created by Gilson Gil on 4/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ProfessionalsEmptyCell: UITableViewCell {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    selectionStyle = .None
  }
}

extension ProfessionalsEmptyCell {
  static func reuseIdentifier() -> String {
    return NSStringFromClass(classForCoder())
  }
}

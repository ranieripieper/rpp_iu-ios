//
//  InviteViewController.swift
//  Iu
//
//  Created by Gilson Gil on 4/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class InviteViewController: UIViewController {
  weak var containerDelegate: ContainerDelegate?
  
  override func loadView() {
    let inviteView = InviteView()
    inviteView.delegate = self
    view = inviteView
    AnswersHelper.pageView("iOS;iu.;Invite")
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension InviteViewController: InviteViewDelegate {
  func inviteViewInvite() {
    let activityViewController = UIActivityViewController(activityItems: [Strings.InviteShare, NSURL(string: Constants.iuProAppStoreLink)!], applicationActivities: nil)
    activityViewController.completionWithItemsHandler = { media, success, _, _ in
      if success, let media = media {
        AnswersHelper.invite(media)
      }
    }
    presentViewController(activityViewController, animated: true, completion: nil)
  }
  
  func inviteViewDismiss() {
    containerDelegate?.goToLogin()
  }
}

extension InviteViewController: ContainerProtocol {}

//
//  InviteView.swift
//  Iu
//
//  Created by Gilson Gil on 4/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol InviteViewDelegate: class {
  func inviteViewInvite()
  func inviteViewDismiss()
}

final class InviteView: UIView {
  weak var delegate: InviteViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    let inviteAnimationView = InviteAnimationView()
    addSubview(inviteAnimationView)
    
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.textAlignment = .Center
    label.textColor = UIColor.iuLightGreyColor()
    label.text = Strings.InviteLabel
    addSubview(label)
    
    let inviteButton = UIButton()
    inviteButton.translatesAutoresizingMaskIntoConstraints = false
    inviteButton.backgroundColor = UIColor.iuUglyPinkColor()
    inviteButton.titleLabel?.font = UIFont.iuBlackFont(18)
    inviteButton.setTitle(Strings.InviteButton, forState: .Normal)
    inviteButton.setTitleColor(UIColor.iuGreyishColor(), forState: .Highlighted)
    inviteButton.layer.cornerRadius = 8
    inviteButton.addTarget(self, action: #selector(InviteView.inviteButtonTapped), forControlEvents: .TouchUpInside)
    addSubview(inviteButton)
    
    let inviteLaterButton = UIButton()
    inviteLaterButton.translatesAutoresizingMaskIntoConstraints = false
    inviteLaterButton.titleLabel?.font = UIFont.iuBlackFont(14)
    inviteLaterButton.setTitleColor(UIColor.iuLightBlueGreyColor(), forState: .Normal)
    inviteLaterButton.setTitleColor(UIColor.iuBlueyGreyColor(), forState: .Highlighted)
    inviteLaterButton.setTitle(Strings.InviteLaterButton, forState: .Normal)
    inviteLaterButton.addTarget(self, action: #selector(InviteView.inviteLaterButtonTapped), forControlEvents: .TouchUpInside)
    addSubview(inviteLaterButton)
    
    let margin: CGFloat
    let largeMargin: CGFloat
    let buttonHeight: CGFloat
    if UIScreen.mainScreen().bounds.height <= 480 {
      margin = 20
      largeMargin = 30
      buttonHeight = 52
      label.font = UIFont.iuBlackFont(15)
    } else {
      margin = 30
      largeMargin = 60
      buttonHeight = 64
      label.font = UIFont.iuBlackFont(18)
    }
    let metrics = ["margin": margin, "largeMargin": largeMargin, "buttonHeight": buttonHeight]
    let views = ["inviteAnimationView": inviteAnimationView, "label": label, "inviteButton": inviteButton, "inviteLaterButton": inviteLaterButton]
    var horizontalConstraints = [
      NSLayoutConstraint(item: inviteAnimationView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: inviteAnimationView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 108)
    ]
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[label]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[inviteButton]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[inviteLaterButton]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[inviteAnimationView(111)]-margin-[label]-largeMargin-[inviteButton(buttonHeight)]-margin-[inviteLaterButton]", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: label, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    addConstraints(verticalConstraints)
  }
}

extension InviteView {
  func inviteButtonTapped() {
    delegate?.inviteViewInvite()
  }
  
  func inviteLaterButtonTapped() {
    delegate?.inviteViewDismiss()
  }
}

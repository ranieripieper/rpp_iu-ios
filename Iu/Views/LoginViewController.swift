//
//  LoginViewController.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {
  weak var containerDelegate: ContainerDelegate?
  
  override func loadView() {
    let loginView = LoginView()
    loginView.delegate = self
    view = loginView
    AnswersHelper.pageView("iOS;iu.;Login")
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LoginViewController: LoginViewDelegate {
  func loginButtonPressedOnLoginView(loginView: LoginView) {
    if let phone = loginView.phone {
      ParseHelper.searchPhone(phone) { [weak self] inner in
        do {
          let professionals = try inner()
          if professionals.count > 1 {
            self?.containerDelegate?.goToProfessionals(professionals)
          } else if professionals.count == 1 {
            self?.containerDelegate?.goToClasses(professionals.first!)
          } else {
            self?.containerDelegate?.goToInvite()
          }
        } catch let parseError as ParseError {
          if parseError == .NotFound {
            self?.containerDelegate?.goToInvite()
          } else {
            loginView.presentError(parseError)
          }
        } catch let error as NSError {
          dispatch_async(dispatch_get_main_queue()) { [weak self] in
            let alertController = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self?.presentViewController(alertController, animated: true, completion: nil)
          }
        }
      }
    }
  }
}

extension LoginViewController: ContainerProtocol {}

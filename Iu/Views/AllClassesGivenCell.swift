//
//  AllClassesGivenCell.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/30/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class AllClassesGivenCell: UITableViewCell {
  private let monthLabel = UILabel()
  private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: DatesFlowLayout(style: .Dark))
  private var collectionViewHeightConstraint: NSLayoutConstraint!
  private var viewModel: AllClassesGivenCellViewModel?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.clearColor()
    
    monthLabel.translatesAutoresizingMaskIntoConstraints = false
    monthLabel.textColor = UIColor.iuLightBlueGreyColor()
    monthLabel.textAlignment = .Center
    monthLabel.font = UIFont.iuBlackFont(24)
    addSubview(monthLabel)
    
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.backgroundColor = UIColor.iuAlmostWhiteColor()
    collectionView.dataSource = self
    collectionView.delegate = self
    addSubview(collectionView)
    
    let metrics = ["margin": 20]
    let views = ["monthLabel": monthLabel, "collectionView": collectionView]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[monthLabel]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: collectionView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 280),
      NSLayoutConstraint(item: collectionView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[monthLabel]-margin-[collectionView]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    collectionViewHeightConstraint = NSLayoutConstraint(item: collectionView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
    collectionView.addConstraint(collectionViewHeightConstraint)
  }
  
  func update(viewModel: AllClassesGivenCellViewModel) {
    self.viewModel = viewModel
    monthLabel.text = viewModel.month.capitalizedString
    viewModel.dates.forEach {
      collectionView.registerClass($0.cellClass, forCellWithReuseIdentifier: $0.reuseIdentifier)
    }
    collectionView.reloadData()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension AllClassesGivenCell: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let count = viewModel?.dates.count ?? 0
    let lines = Int((count - 1) / 3) + 1
    collectionViewHeightConstraint.constant = CGFloat(lines * 20)
    return count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let configurator = viewModel!.dates[indexPath.item]
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    return cell
  }
}

extension AllClassesGivenCell: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let itemsInLine = numberOfItemsInLine(indexPath)
    let padding = (itemsInLine - 1) * 8
    let width = floor((collectionView.bounds.width - padding) / CGFloat(min(3, itemsInLine)))
    return CGSize(width: width, height: 12)
  }
  
  func numberOfItemsInLine(indexPath: NSIndexPath) -> CGFloat {
    guard let count = viewModel?.dates.count where count > 0 else {
      return 0
    }
    let lines = Int((count - 1) / 3) + 1
    let line = Int(indexPath.item / 3) + 1
    if line != lines {
      return 3
    } else {
      let rest = count % 3
      if rest == 0 {
        return 3
      } else {
        return CGFloat(rest)
      }
    }
  }
}

extension AllClassesGivenCell: Updatable {
  typealias ViewModel = AllClassesGivenCellViewModel
}

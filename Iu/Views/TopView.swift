//
//  TopView.swift
//  Iu
//
//  Created by Gilson Gil on 2/17/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TopView: UIView {
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.iuDarkGreyColor()
    let logoImage = UIImage.iuLogo()
    let logoImageView = UIImageView(image: logoImage)
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(logoImageView)
    
    let horizontalConstraint = NSLayoutConstraint(item: logoImageView, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .RightMargin, multiplier: 1, constant: 0)
    addConstraint(horizontalConstraint)
    let verticalConstraint = NSLayoutConstraint(item: logoImageView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
    addConstraint(verticalConstraint)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

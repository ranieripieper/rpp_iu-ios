//
//  ContainerView.swift
//  Iu
//
//  Created by Gilson Gil on 2/17/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ContainerView: UIView {
  private let topView = TopView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    addSubview(topView)
    
    let metrics = ["topViewHeight": Constants.topViewHeight]
    let views = ["topView": topView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[topView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[topView(topViewHeight)]", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
  
  override func addSubview(view: UIView) {
    super.addSubview(view)
    bringSubviewToFront(topView)
  }
}

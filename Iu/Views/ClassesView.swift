//
//  ClassesView.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol ClassesViewDelegate: class {
  func logoutPressedOnClassesView(classesView: ClassesView)
  func showAllClassesGiven()
}

final class ClassesView: UIView {
  private let margin: CGFloat = 20
  private let logoutHeight: CGFloat = 72
  private let minimumLogoutLimit: CGFloat = 0.4
  private let logoutBackgroundOffAlpha: CGFloat = 0.6
  private let loadingViewSize: CGFloat = 60
  
  private let introLabel = UILabel()
  private let countLabel = UILabel()
  private let classesLabel = UILabel()
  private let expireDateLabel = UILabel()
  private let logoutLabel = UILabel()
  private let classesGivenView: ClassesGivenView
  private let professional: Professional
  private var loadingView: LoadingView?

  weak var delegate: ClassesViewDelegate?
  private var classesViewModel: ClassesViewModel?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(professional: Professional) {
    if let classesViewModel = ClassesViewModel(professional: professional) {
      classesGivenView = ClassesGivenView(viewModel: classesViewModel.classesGivenViewModel)
      self.classesViewModel = classesViewModel
      self.professional = professional
    } else {
      classesGivenView = ClassesGivenView()
      classesViewModel = nil
      self.professional = professional
      loadingView = LoadingView()
    }
    super.init(frame: .zero)
    setUp()
    configure(classesViewModel)
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    let scrollView = UIScrollView()
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.backgroundColor = UIColor.clearColor()
    scrollView.delegate = self
    
    let scrollViewContentView = UIView()
    scrollViewContentView.translatesAutoresizingMaskIntoConstraints = false
    scrollViewContentView.backgroundColor = UIColor.iuAlmostWhiteColor()
    
    introLabel.translatesAutoresizingMaskIntoConstraints = false
    introLabel.textColor = UIColor.iuLightGreyColor()
    introLabel.font = UIFont.iuBlackFont(18)
    introLabel.textAlignment = .Center
    
    countLabel.translatesAutoresizingMaskIntoConstraints = false
    countLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    countLabel.textColor = UIColor.iuUglyPinkColor()
    countLabel.font = UIFont.iuBlackFont(160)
    countLabel.textAlignment = .Center
    
    classesLabel.translatesAutoresizingMaskIntoConstraints = false
    classesLabel.textColor = UIColor.iuLightGreyColor()
    classesLabel.textAlignment = .Center
    classesLabel.font = UIFont.iuBlackFont(18)
    classesLabel.numberOfLines = 0
    
    expireDateLabel.translatesAutoresizingMaskIntoConstraints = false
    expireDateLabel.textColor = UIColor.iuGreyishColor()
    expireDateLabel.font = UIFont.iuBlackFont(36)
    expireDateLabel.textAlignment = .Center
    
    classesGivenView.translatesAutoresizingMaskIntoConstraints = false
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(ClassesView.classesGivenTapped))
    classesGivenView.addGestureRecognizer(tap)
    
    logoutLabel.translatesAutoresizingMaskIntoConstraints = false
    logoutLabel.font = UIFont.iuBlackFont(18)
    logoutLabel.text = Strings.ClassesLogout
    logoutLabel.textAlignment = .Center
    logoutLabel.textColor = UIColor.iuAlmostWhiteColor()
    logoutLabel.backgroundColor = UIColor.iuBlueyGreyColor()
    
    addSubview(logoutLabel)
    addSubview(scrollView)
    scrollView.addSubview(scrollViewContentView)
    scrollViewContentView.addSubview(introLabel)
    scrollViewContentView.addSubview(countLabel)
    scrollViewContentView.addSubview(classesLabel)
    scrollViewContentView.addSubview(expireDateLabel)
    scrollViewContentView.addSubview(classesGivenView)
    
    let views = ["scrollView": scrollView, "scrollViewContentView": scrollViewContentView, "introLabel": introLabel, "countLabel": countLabel, "classesLabel": classesLabel, "expireDateLabel": expireDateLabel, "classesGivenView": classesGivenView, "logoutLabel": logoutLabel]
    let metrics = ["margin": margin, "logoutHeight": logoutHeight]
    
    var scrollViewHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(scrollViewHorizontalConstraints)
    
    scrollViewHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(scrollViewHorizontalConstraints)
    
    scrollViewHorizontalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0)]
    addConstraints(scrollViewHorizontalConstraints)
    
    var scrollViewVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(scrollViewVerticalConstraints)
    
    scrollViewVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(scrollViewVerticalConstraints)
    
    scrollViewVerticalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .Height, multiplier: 1, constant: 1)]
    addConstraints(scrollViewVerticalConstraints)
    
    var labelCenterXConstraint = NSLayoutConstraint(item: introLabel, attribute: .CenterX, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterX, multiplier: 1, constant: 0)
    scrollViewContentView.addConstraint(labelCenterXConstraint)
    
    labelCenterXConstraint = NSLayoutConstraint(item: countLabel, attribute: .CenterX, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterX, multiplier: 1, constant: 0)
    scrollViewContentView.addConstraint(labelCenterXConstraint)
    
    labelCenterXConstraint = NSLayoutConstraint(item: classesLabel, attribute: .CenterX, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterX, multiplier: 1, constant: 0)
    scrollViewContentView.addConstraint(labelCenterXConstraint)
    
    let labelCenterYConstraint = NSLayoutConstraint(item: classesLabel, attribute: .CenterY, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterY, multiplier: 1, constant: 0)
    scrollViewContentView.addConstraint(labelCenterYConstraint)
    
    labelCenterXConstraint = NSLayoutConstraint(item: expireDateLabel, attribute: .CenterX, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterX, multiplier: 1, constant: 0)
    scrollViewContentView.addConstraint(labelCenterXConstraint)
    
    let givenHorizontalConstraints = [NSLayoutConstraint(item: classesGivenView, attribute: .CenterX, relatedBy: .Equal, toItem: scrollViewContentView, attribute: .CenterX, multiplier: 1, constant: 0)]
    scrollViewContentView.addConstraints(givenHorizontalConstraints)
    
    let labelCenterYConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[introLabel][countLabel(180)][classesLabel]-margin-[expireDateLabel]-margin-[classesGivenView]->=margin-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(labelCenterYConstraints)
    
    let logoutXConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[logoutLabel]|", options: [], metrics: metrics, views: views)
    addConstraints(logoutXConstraints)
    
    let logoutYConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[logoutLabel(logoutHeight)]|", options: [], metrics: metrics, views: views)
    addConstraints(logoutYConstraints)
    
    guard let loadingView = loadingView else {
      return
    }
    
    addSubview(loadingView)
    loadingView.translatesAutoresizingMaskIntoConstraints = false
    
    let loadingHorizontalConstraint = NSLayoutConstraint(item: loadingView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
    addConstraint(loadingHorizontalConstraint)
    let loadingVerticalConstraint = NSLayoutConstraint(item: loadingView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
    addConstraint(loadingVerticalConstraint)
    loadingView.startAnimating()
  }

  func configure(classesViewModel: ClassesViewModel?) {
    guard let classesViewModel = classesViewModel else {
      ClassesViewModel.viewModel(professional) { [weak self] inner in
        do {
          let classesVM = try inner()
          self?.classesViewModel = classesVM
          self?.configure(classesVM)
          self?.loadingView?.endAnimating()
          self?.loadingView?.removeFromSuperview()
          self?.loadingView = nil
        } catch {
          
        }
      }
      return
    }
    introLabel.text = classesViewModel.welcomeString
    countLabel.text = classesViewModel.counterString
    classesLabel.text = classesViewModel.descriptionString
    expireDateLabel.text = classesViewModel.dateString
  }
  
  func classesGivenTapped() {
    if let viewModel = classesGivenView.viewModel where viewModel.hasMore {
      delegate?.showAllClassesGiven()
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ClassesView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let factor = min(1, max(minimumLogoutLimit, scrollView.contentOffset.y / logoutHeight))
    logoutLabel.textColor = UIColor.iuAlmostWhiteColor().colorWithAlphaComponent(factor)
    if factor < 1 {
      logoutLabel.backgroundColor = logoutLabel.backgroundColor?.colorWithAlphaComponent(logoutBackgroundOffAlpha)
    } else {
      logoutLabel.backgroundColor = logoutLabel.backgroundColor?.colorWithAlphaComponent(1)
    }
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if scrollView.contentOffset.y > logoutHeight {
      delegate?.logoutPressedOnClassesView(self)
    }
  }
}

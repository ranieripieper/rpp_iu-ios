//
//  AllClassesGivenViewController.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/30/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class AllClassesGivenViewController: UIViewController {
  private let allClassesGivenView: AllClassesGivenView
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(professionalId: String) {
    allClassesGivenView = AllClassesGivenView(professionalId: professionalId)
    super.init(nibName: nil, bundle: nil)
    allClassesGivenView.delegate = self
  }
  
  override func loadView() {
    view = allClassesGivenView
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension AllClassesGivenViewController: AllClassesGivenViewDelegate {
  func pop() {
    navigationController?.popViewControllerAnimated(true)
  }
}

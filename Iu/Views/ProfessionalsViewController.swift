//
//  ProfessionalsViewController.swift
//  Iu
//
//  Created by Gilson Gil on 4/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ProfessionalsViewController: UIViewController {
  private let professionalsView: ProfessionalsView
  
  weak var containerDelegate: ContainerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(professionalsViewModel: ProfessionalsViewModel) {
    professionalsView = ProfessionalsView(professionalsViewModel: professionalsViewModel)
    super.init(nibName: nil, bundle: nil)
    professionalsView.delegate = self
  }
  
  override func loadView() {
    view = professionalsView
    AnswersHelper.pageView("iOS;iu.;ProfessionalsList")
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ProfessionalsViewController: ProfessionalsViewDelegate {
  func goToProfessional(professional: Professional) {
    let classesViewController = ClassesViewController(professional: professional)
    classesViewController.containerDelegate = containerDelegate
    navigationController?.pushViewController(classesViewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
  }
  
  func logoutPressedOnProfessionalsView(professionalsView: ProfessionalsView) {
    containerDelegate?.goToLogin()
  }
}

extension ProfessionalsViewController: ContainerProtocol {}

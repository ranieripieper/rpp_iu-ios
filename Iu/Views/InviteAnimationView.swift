//
//  InviteAnimationView.swift
//  Iu
//
//  Created by Gilson Gil on 4/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class InviteAnimationView: UIImageView {
  private var timer: NSTimer?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    
    image = UIImage.iuEnvelop1()
    animationImages = [UIImage.iuEnvelop1()!, UIImage.iuEnvelop2()!, UIImage.iuEnvelop3()!, UIImage.iuEnvelop4()!]
    contentMode = .Bottom
    animationDuration = 1
    animationRepeatCount = 1
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1 * NSEC_PER_SEC)), dispatch_get_main_queue()) { [weak self] in
      self?.image = UIImage.iuEnvelop4()
      self?.startAnimating()
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(2 * NSEC_PER_SEC)), dispatch_get_main_queue()) { [weak self] in
        if let weakSelf = self {
          weakSelf.wiggle()
          weakSelf.timer = NSTimer.scheduledTimerWithTimeInterval(5, target: weakSelf, selector: #selector(InviteAnimationView.wiggle), userInfo: nil, repeats: true)
        }
      }
    }
  }
  
  func wiggle() {
    let animation = CAKeyframeAnimation(keyPath: "transform.rotation")
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    animation.duration = 0.4
    animation.values = [-M_PI/10, M_PI/10, -M_PI/10, M_PI/10, -M_PI/12, M_PI/12, -M_PI/20, M_PI/20, 0]
    layer.addAnimation(animation, forKey: "shake")
  }
}

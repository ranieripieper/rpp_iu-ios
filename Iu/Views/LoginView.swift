//
//  LoginView.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol LoginViewDelegate: class {
  func loginButtonPressedOnLoginView(loginView: LoginView)
}

final class LoginView: UIView {
  private let loadingViewSize: CGFloat = 60
  private let margin: CGFloat = 32
  private let textFieldPadding: CGFloat = 12
  private lazy var errorImageView: UIImageView = {
    let errorImageView = UIImageView(image: UIImage.iuOps())
    errorImageView.translatesAutoresizingMaskIntoConstraints = false
    errorImageView.contentMode = .Center
    
    return errorImageView
  }()
  private lazy var errorLabel: UILabel = {
    let errorLabel = UILabel()
    errorLabel.translatesAutoresizingMaskIntoConstraints = false
    errorLabel.font = UIFont.iuBlackFont(14)
    errorLabel.textColor = UIColor.iuLightGreyColor()
    errorLabel.textAlignment = .Center
    errorLabel.numberOfLines = 0
    errorLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    errorLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    
    return errorLabel
  }()
  private let container = UIView()
  private let textField = NBTextField()
  private let loginButton = UIButton(type: .Custom)
  private let loadingView = LoadingView()
  
  weak var delegate: LoginViewDelegate?
  var phone: Double? {
    if let text = textField.text, phone = Double(text.cleanString()) {
      return phone
    } else {
      return nil
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    textField.translatesAutoresizingMaskIntoConstraints = false
    textField.placeholder = Strings.LoginTypePhone
    textField.font = UIFont.iuHeavyFont(16)
    textField.textColor = UIColor.iuLightGreyColor()
    textField.tintColor = UIColor.iuLightBlueGreyColor()
    textField.countryCode = "BR"
    textField.delegate = self
    textField.backgroundColor = UIColor.whiteColor()
    textField.keyboardType = .PhonePad
    textField.layer.cornerRadius = 8
    textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    textField.leftViewMode = .Always
    textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldPadding, height: 0))
    textField.rightViewMode = .Always
    
    loginButton.translatesAutoresizingMaskIntoConstraints = false
    loginButton.backgroundColor = UIColor.iuUglyPinkColor()
    loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    loginButton.titleLabel?.font = UIFont.iuBlackFont(18)
    loginButton.setTitle(Strings.LoginButton, forState: .Normal)
    loginButton.addTarget(self, action: #selector(LoginView.loginButtonPressed), forControlEvents: .TouchUpInside)
    loginButton.layer.cornerRadius = 8
    
    container.translatesAutoresizingMaskIntoConstraints = false
    container.addSubview(textField)
    container.addSubview(loginButton)
    
    let views = ["textField": textField, "loginButton": loginButton, "container": container]
    let metrics = ["margin": margin, "height": 64]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[textField]-margin-|", options: [], metrics: metrics, views: views)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[textField(height)]-margin-[loginButton(height)]-margin-|", options: [], metrics: metrics, views: views)
    
    container.addConstraints(horizontalConstraints)
    container.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[loginButton]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    
    addSubview(container)
    
    horizontalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)]
    addConstraints(horizontalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: container, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|->=margin-[container]->=margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    textField.becomeFirstResponder()
    loginButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, UIScreen.mainScreen().bounds.width, 0)
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    endEditing(true)
  }
  
  func loginButtonPressed() {
    endEditing(true)
    startLoading()
    delegate?.loginButtonPressedOnLoginView(self)
  }
  
  private func startLoading() {
    textField.userInteractionEnabled = false
    loginButton.userInteractionEnabled = false
    
    loadingView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(loadingView)
    
    let metrics = ["loadingViewSize": loadingViewSize]
    let views: [String: UIView]
    if errorLabel.superview == nil || UIScreen.mainScreen().bounds.height <= 480 {
      views = ["loadingView": loadingView, "referenceView": loginButton]
    } else {
      views = ["loadingView": loadingView, "referenceView": errorLabel]
    }
    let horizontalConstraints = [
      NSLayoutConstraint(item: loadingView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: loadingView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: loadingViewSize)
    ]
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[referenceView]-30-[loadingView(loadingViewSize)]", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5 * NSEC_PER_MSEC)), dispatch_get_main_queue()) { [weak self] in
      self?.loadingView.startAnimating()
    }
  }
  
  private func stopLoading() {
    textField.userInteractionEnabled = true
    loginButton.userInteractionEnabled = true
    
    loadingView.endAnimating()
    loadingView.removeFromSuperview()
  }
  
  private func showLoginButton(show: Bool) {
    if show {
      presentLoginButton()
    } else {
      hideLoginButton()
    }
  }
  
  private func presentLoginButton() {
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.defaultAnimationDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: { [unowned self] in
      self.loginButton.transform = CGAffineTransformIdentity
      }, completion: nil)
  }
  
  private func hideLoginButton() {
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: Constants.defaultAnimationDelay, usingSpringWithDamping: Constants.defaultAnimationDamping, initialSpringVelocity: Constants.defaultAnimationInitialVelocity, options: .CurveEaseInOut, animations: { [unowned self] in
      self.loginButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, self.bounds.width, 0)
      }, completion: nil)
  }
  
  func presentError(error: ParseError) {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      guard let weakSelf = self else {
        return
      }
      weakSelf.stopLoading()
      switch error {
      case .NotFound:
        let errorImageView = weakSelf.errorImageView
        weakSelf.errorImageView.hidden = false
        weakSelf.addSubview(errorImageView)
        
        weakSelf.errorLabel.removeFromSuperview()
        let errorLabel = weakSelf.errorLabel
        errorLabel.text = Strings.LoginNotFoundError
        errorLabel.hidden = false
        weakSelf.addSubview(errorLabel)
        
        let metrics = ["topViewHeight": Constants.topViewHeight]
        let views = ["container": weakSelf.container, "errorImageView": errorImageView, "errorLabel": errorLabel]
        var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[errorImageView]|", options: [], metrics: nil, views: views)
        weakSelf.addConstraints(horizontalConstraints)
        horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-30-[errorLabel]-30-|", options: [], metrics: nil, views: views)
        weakSelf.addConstraints(horizontalConstraints)
        var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topViewHeight-[errorImageView][container]", options: [], metrics: metrics, views: views)
        weakSelf.addConstraints(verticalConstraints)
        verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[container]-8-[errorLabel]", options: [], metrics: nil, views: views)
        weakSelf.addConstraints(verticalConstraints)
      case .Network:
        weakSelf.errorImageView.hidden = true
        weakSelf.errorLabel.removeFromSuperview()
        let errorLabel = weakSelf.errorLabel
        errorLabel.text = Strings.LoginNetworkError
        errorLabel.hidden = false
        weakSelf.addSubview(errorLabel)
        
        let views = ["errorLabel": errorLabel]
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-30-[errorLabel]-30-|", options: [], metrics: nil, views: views)
        weakSelf.addConstraints(horizontalConstraints)
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[errorLabel]-20-|", options: [], metrics: nil, views: views)
        weakSelf.addConstraints(verticalConstraints)
      default:
        break
      }
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LoginView: UITextFieldDelegate {
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    guard let text = textField.text else {
      return true
    }
    let startIndex = text.startIndex.advancedBy(range.location)
    let endIndex = startIndex.advancedBy(range.length)
    let range = startIndex..<endIndex
    let newString = text.stringByReplacingCharactersInRange(range, withString: string)
    let cleanString = newString.cleanString()
    showLoginButton(cleanString.characters.count >= Constants.minimumPhoneLength)
    if cleanString.characters.count > Constants.maximumPhoneLength {
      return false
    }
    if newString.characters.count == 0 {
      textField.font = UIFont.iuHeavyFont(16)
      textField.textColor = UIColor.iuLightGreyColor()
    } else {
      textField.font = UIFont.iuBlackFont(18)
      textField.textColor = UIColor.iuPinkishGreyColor()
    }
    return true
  }
}

extension String {
  func cleanString() -> String {
    let cleanString = self
      .stringByReplacingOccurrencesOfString(" ", withString: "")
      .stringByReplacingOccurrencesOfString("(", withString: "")
      .stringByReplacingOccurrencesOfString(")", withString: "")
      .stringByReplacingOccurrencesOfString("-", withString: "")
    
    return cleanString
  }
}

//
//  ClassesViewController.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ClassesViewController: UIViewController {
  private let classesView: ClassesView
  private let professional: Professional
  
  weak var containerDelegate: ContainerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(professional: Professional) {
    self.professional = professional
    classesView = ClassesView(professional: professional)
    super.init(nibName: nil, bundle: nil)
    classesView.delegate = self
  }
  
  override func loadView() {
    view = classesView
    AnswersHelper.pageView("iOS;iu.;Professional")
  }
  
  private func goToLogin() {
    ParseHelper.signout()
    containerDelegate?.goToLogin()
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ClassesViewController: ClassesViewDelegate {
  func logoutPressedOnClassesView(classesView: ClassesView) {
    goToLogin()
  }
  
  func showAllClassesGiven() {
    let allClassesGivenViewController = AllClassesGivenViewController(professionalId: professional.objectId)
    navigationController?.pushViewController(allClassesGivenViewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
  }
}

extension ClassesViewController: ContainerProtocol {}

//
//  ProfessionalsView.swift
//  Iu
//
//  Created by Gilson Gil on 4/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol ProfessionalsViewDelegate: class {
  func goToProfessional(professional: Professional)
  func logoutPressedOnProfessionalsView(professionalsView: ProfessionalsView)
}

final class ProfessionalsView: UIView {
  private let margin: CGFloat = 24
  private let textFieldPadding: CGFloat = 12
  private let logoutHeight: CGFloat = 72
  private let minimumLogoutLimit: CGFloat = 0.4
  private let logoutBackgroundOffAlpha: CGFloat = 0.6
  private let loadingViewSize: CGFloat = 60
  
  private let tableView = UITableView()
  private let logoutLabel = UILabel()
  private let loadingView = LoadingView()
  
  private let professionalsViewModel: ProfessionalsViewModel
  weak var delegate: ProfessionalsViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(professionalsViewModel: ProfessionalsViewModel) {
    self.professionalsViewModel = professionalsViewModel
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.iuAlmostWhiteColor()
    
    logoutLabel.translatesAutoresizingMaskIntoConstraints = false
    logoutLabel.font = UIFont.iuBlackFont(18)
    logoutLabel.text = Strings.ClassesLogout
    logoutLabel.textAlignment = .Center
    logoutLabel.textColor = UIColor.iuAlmostWhiteColor()
    logoutLabel.backgroundColor = UIColor.iuBlueyGreyColor()
    addSubview(logoutLabel)
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.clearColor()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = 50
    tableView.separatorColor = UIColor.iuLightGreyColor()
    professionalsViewModel.professionalItems.forEach {
      tableView.registerClass($0.cellClass, forCellReuseIdentifier: $0.reuseIdentifier)
    }
    tableView.registerClass(ProfessionalsEmptyCell.classForCoder(), forCellReuseIdentifier: ProfessionalsEmptyCell.reuseIdentifier())
    addSubview(tableView)
    
    let metrics = ["logoutHeight": logoutHeight]
    let views = ["tableView": tableView, "logoutLabel": logoutLabel]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
    
    let logoutXConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[logoutLabel]|", options: [], metrics: metrics, views: views)
    addConstraints(logoutXConstraints)
    
    let logoutYConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[logoutLabel(logoutHeight)]|", options: [], metrics: metrics, views: views)
    addConstraints(logoutYConstraints)
  }
  
  private func startLoading() {
    loadingView.startAnimating()
  }
  
  private func stopLoading() {
    loadingView.endAnimating()
    loadingView.removeFromSuperview()
  }
}

extension ProfessionalsView: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return professionalsViewModel.professionalItems.count ?? 0
    } else {
      return 1
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return UITableViewAutomaticDimension
    } else {
      if let window = window {
        let possibleHeight = window.bounds.height - (convertPoint(tableView.frame.origin, toView: window).y + CGFloat(tableView.numberOfRowsInSection(0)) * 80)
        if possibleHeight < 0 {
          return 0
        } else {
          return possibleHeight
        }
      }
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    guard indexPath.section == 0 else {
      return tableView.dequeueReusableCellWithIdentifier(ProfessionalsEmptyCell.reuseIdentifier(), forIndexPath: indexPath)
    }
    let configurator = professionalsViewModel.professionalItems[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    return cell
  }
}

extension ProfessionalsView: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    delegate?.goToProfessional(professionalsViewModel.professionals[indexPath.row])
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
}

extension ProfessionalsView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let factor = min(1, max(minimumLogoutLimit, scrollView.contentOffset.y / logoutHeight))
    logoutLabel.textColor = UIColor.iuAlmostWhiteColor().colorWithAlphaComponent(factor)
    if factor < 1 {
      logoutLabel.backgroundColor = logoutLabel.backgroundColor?.colorWithAlphaComponent(logoutBackgroundOffAlpha)
    } else {
      logoutLabel.backgroundColor = logoutLabel.backgroundColor?.colorWithAlphaComponent(1)
    }
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    let shouldLogout = scrollView.contentOffset.y + scrollView.bounds.height > scrollView.contentSize.height + logoutHeight
    if shouldLogout {
      delegate?.logoutPressedOnProfessionalsView(self)
    }
  }
}

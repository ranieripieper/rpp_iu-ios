//
//  ContainerViewController.swift
//  Iu
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol ContainerDelegate: class {
  func goToLogin()
  func goToInvite()
  func goToClasses(professional: Professional)
  func goToProfessionals(professionals: [Professional])
}

protocol ContainerProtocol {
  weak var containerDelegate: ContainerDelegate? { get set }
}

final class ContainerViewController: UINavigationController {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: nil, bundle: nil)
  }
  
  init() {
    super.init(navigationBarClass: NavigationBar.self, toolbarClass: nil)
    view.backgroundColor = UIColor.iuAlmostWhiteColor()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    ParseHelper.isLoggedIn { [weak self] loggedIn in
      if loggedIn {
        let loadingView: LoadingView?
        if let weakSelf = self {
          loadingView = LoadingView()
          loadingView!.translatesAutoresizingMaskIntoConstraints = false
          weakSelf.view.addSubview(loadingView!)
          let horizontalConstraint = NSLayoutConstraint(item: loadingView!, attribute: .CenterX, relatedBy: .Equal, toItem: weakSelf.view, attribute: .CenterX, multiplier: 1, constant: 0)
          weakSelf.view.addConstraint(horizontalConstraint)
          let verticalConstraint = NSLayoutConstraint(item: loadingView!, attribute: .CenterY, relatedBy: .Equal, toItem: weakSelf.view, attribute: .CenterY, multiplier: 1, constant: 0)
          weakSelf.view.addConstraint(verticalConstraint)
          loadingView!.startAnimating()
        } else {
          loadingView = nil
        }
        ParseHelper.getInstructors { inner in
          do {
            let professionals = try inner()
            if professionals.count > 1 {
              self?.loadProfessionals(professionals)
            } else if let professional = professionals.first {
              self?.loadClasses(professional)
            }
          } catch {
            self?.loadLogin()
          }
          loadingView?.endAnimating()
          loadingView?.removeFromSuperview()
        }
      } else {
        self?.loadLogin()
      }
    }
    VersionChecker.hasUpdate { [weak self] tuple in
      if tuple.update, let view = self?.view {
        AlertView.presentVersionAlert(inView: view, topPadding: self!.navigationBar.bounds.height, belowSubview: self!.navigationBar, mandatory: tuple.force)
      }
    }
  }
  
  private func loadProfessionals(professionals: [Professional]) {
    let professionalsViewModel = ProfessionalsViewModel(professionals: professionals)
    let professionalsViewController = ProfessionalsViewController(professionalsViewModel: professionalsViewModel)
    professionalsViewController.containerDelegate = self
    addChildViewController(professionalsViewController)
    viewControllers = [professionalsViewController]
  }
  
  private func loadClasses(professional: Professional) {
    let classesViewController = ClassesViewController(professional: professional)
    classesViewController.containerDelegate = self
    addChildViewController(classesViewController)
    viewControllers = [classesViewController]
  }
  
  private func loadLogin() {
    let loginViewController = LoginViewController()
    loginViewController.containerDelegate = self
    addChildViewController(loginViewController)
    viewControllers = [loginViewController]
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
}

extension ContainerViewController: ContainerDelegate {
  func goToLogin() {
    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
      UIView.animateWithDuration(0.3, animations: {
        self.viewControllers.forEach {
          $0.view.alpha = 0
        }
        }, completion: { finished in
          self.viewControllers.forEach {
            $0.removeFromParentViewController()
            $0.view.removeFromSuperview()
          }
          let loginViewController = LoginViewController()
          loginViewController.containerDelegate = self
          self.addChildViewController(loginViewController)
          self.viewControllers = [loginViewController]
      })
    }
  }
  
  func goToInvite() {
    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
      UIView.animateWithDuration(0.3, animations: {
        self.viewControllers.forEach {
          $0.view.alpha = 0
        }
        }, completion: { finished in
          self.viewControllers.forEach {
            $0.removeFromParentViewController()
            $0.view.removeFromSuperview()
          }
          let inviteViewController = InviteViewController()
          inviteViewController.containerDelegate = self
          self.addChildViewController(inviteViewController)
          self.viewControllers = [inviteViewController]
      })
    }
  }
  
  func goToClasses(professional: Professional) {
    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
      UIView.animateWithDuration(0.3, animations: {
        self.viewControllers.forEach {
          $0.view.alpha = 0
        }
        }, completion: { finished in
          self.viewControllers.forEach {
            $0.removeFromParentViewController()
            $0.view.removeFromSuperview()
          }
          let classesViewController = ClassesViewController(professional: professional)
          classesViewController.containerDelegate = self
          self.addChildViewController(classesViewController)
          self.viewControllers = [classesViewController]
      })
    }
  }
  
  func goToProfessionals(professionals: [Professional]) {
    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
      UIView.animateWithDuration(0.3, animations: {
        self.viewControllers.forEach {
          $0.view.alpha = 0
        }
        }, completion: { finished in
          self.viewControllers.forEach {
            $0.removeFromParentViewController()
            $0.view.removeFromSuperview()
          }
          let professionalsViewModel = ProfessionalsViewModel(professionals: professionals)
          let professionalsViewController = ProfessionalsViewController(professionalsViewModel: professionalsViewModel)
          professionalsViewController.containerDelegate = self
          self.addChildViewController(professionalsViewController)
          self.viewControllers = [professionalsViewController]
      })
    }
  }
}

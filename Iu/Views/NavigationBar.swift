//
//  NavigationBar.swift
//  iu.Professor
//
//  Created by Gilson Gil on 2/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NavigationBar: UINavigationBar {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    barTintColor = UIColor.iuDarkGreyColor()
    tintColor = UIColor.whiteColor()
    translucent = false
    
    let logoImage = UIImage.iuLogo()
    let logoImageView = UIImageView(image: logoImage)
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(logoImageView)
    
    let horizontalConstraint = NSLayoutConstraint(item: self, attribute: .RightMargin, relatedBy: .Equal, toItem: logoImageView, attribute: .Right, multiplier: 1, constant: 0)
    addConstraint(horizontalConstraint)
    
    let verticalConstraint = NSLayoutConstraint(item: self, attribute: .CenterY, relatedBy: .Equal, toItem: logoImageView, attribute: .CenterY, multiplier: 1, constant: 0)
    addConstraint(verticalConstraint)
  }
  
  override func sizeThatFits(size: CGSize) -> CGSize {
    return CGSize(width: UIScreen.mainScreen().bounds.width, height: 82)
  }
}

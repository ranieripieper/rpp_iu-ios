//
//  AllClassesGivenViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/30/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct AllClassesGivenViewModel {
  let professionalId: String
  let months: [CellConfiguratorType]
  let currentDates: [NSDate]
  
  static var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "MMMM yyyy"
    return dateFormatter
  }()
  
  init(professionalId: String) {
    self.professionalId = professionalId
    months = []
    currentDates = []
  }
  
  init(professionalId: String, months: [CellConfiguratorType], currentDates: [NSDate]) {
    self.professionalId = professionalId
    self.months = months
    self.currentDates = currentDates
  }
  
  func load(page: Int, completion: (inner: () throws -> AllClassesGivenViewModel) -> ()) {
    ParseHelper.givenClasses(professionalId, page: page) { inner in
      do {
        let dates = try inner()
        guard dates.count > 0 else {
          completion { throw ParseError.Unhandled }
          return
        }
        let allDates = self.currentDates + dates
        let groupedMonths: [[NSDate]] = allDates.groupMonths() ?? []
        let months = groupedMonths.map { month -> CellConfiguratorType in
          return CellConfigurator<AllClassesGivenCell>(viewModel: AllClassesGivenCellViewModel(month: AllClassesGivenViewModel.dateFormatter.stringFromDate(month.first!), dates: month.map { date -> CellConfiguratorType in
            return CellConfigurator<ClassesGivenCell>(viewModel: ClassesGivenCellViewModel(date: date))
            }))
        }
        let newAllClassesGivenViewModel = AllClassesGivenViewModel(professionalId: self.professionalId, months: months, currentDates: allDates)
        completion { return newAllClassesGivenViewModel }
      } catch {
        completion { throw ParseError.Unhandled }
      }
    }
  }
}

extension Array {
  func groupMonths() -> [[NSDate]]? {
    let dates = self.flatMap { date -> NSDate? in
      guard let date = date as? NSDate else {
        return nil
      }
      return date
    }
    var groupedMonths = [[NSDate]]()
    let currentCalendar = NSCalendar.currentCalendar()
    let years = dates.map { date -> Int in
      let components = currentCalendar.components([.Year], fromDate: date)
      return components.year
    }
    let yearsSet = Set(years).sort { $0 > $1 }
    yearsSet.forEach { year in
      let filteredYear = dates.filter { date in
        let components = currentCalendar.components([.Year], fromDate: date)
        return components.year == year
      }
      let months = filteredYear.flatMap { date -> Int in
        let components = currentCalendar.components([.Month], fromDate: date)
        return components.month
      }
      let monthsSet = Set(months).sort { $0 > $1 }
      monthsSet.forEach { month in
        let filteredMonth = filteredYear.filter { date in
          let components = currentCalendar.components([.Month], fromDate: date)
          return components.month == month
        }
        groupedMonths.append(filteredMonth)
      }
    }
    return groupedMonths
  }
}

//
//  ClassesViewModel.swift
//  Iu
//
//  Created by Gilson Gil on 4/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ClassesViewModel {
  private static let dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter
  }()
  
  let welcomeString: String
  let counterString: String
  let descriptionString: String
  let dateString: String
  let classesGivenViewModel: ClassesGivenViewModel
  
  init?(professional: Professional) {
    guard let payment = professional.payment, significantDate = professional.significantDate else {
      return nil
    }
    switch payment {
    case .Before:
      welcomeString = String(format: Strings.ClassesAvailableClasses, arguments: [professional.studentName ?? ""])
      if professional.classesCount == 0 {
        descriptionString = Strings.ClassesNoClasses
      } else if professional.classesCount == 1 {
        descriptionString = Strings.ClassesClass
      } else {
        descriptionString = Strings.ClassesClasses
      }
    case .After:
      welcomeString = String(format: Strings.ClassesUsedClasses, arguments: [professional.studentName ?? ""])
      if professional.classesCount == 0 {
        descriptionString = Strings.ClassesNoPayment
      } else if professional.classesCount == 1 {
        descriptionString = Strings.ClassesPayment
      } else {
        descriptionString = Strings.ClassesPayments
      }
    }
    counterString = String(professional.classesCount)
    dateString = ClassesViewModel.dateFormatter.stringFromDate(significantDate)
    let classes = professional.recentClasses.flatMap { aClass -> NSDate? in
      guard let date = aClass["usedAt"] as? NSDate else {
        return nil
      }
      return date
    }
    classesGivenViewModel = ClassesGivenViewModel(classes: classes, hasMore: classes.count >= 5)
  }
  
  static func viewModel(professional: Professional, completion: (inner: () throws -> ClassesViewModel) -> ()) {
    ParseHelper.getProfessionalSummary(professional.objectId) { inner in
      do {
        let summary = try inner()
        guard let newProfessional = Professional(professional: professional, summary: summary), classesViewModel = ClassesViewModel(professional: newProfessional) else {
          completion { throw ParseError.Unhandled }
          return
        }
        completion { return classesViewModel }
      } catch {
        completion { throw ParseError.Unhandled }
      }
    }
  }
}

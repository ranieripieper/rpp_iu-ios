//
//  ClassesGivenCellViewModel.swift
//  Iu
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ClassesGivenCellViewModel {
  private static let dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd.MM.yy"
    return dateFormatter
  }()
  
  let dateString: String
  
  init(date: NSDate) {
    dateString = ClassesGivenCellViewModel.dateFormatter.stringFromDate(date)
  }
}

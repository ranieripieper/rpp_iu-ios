//
//  ClassesGivenViewModel.swift
//  Iu
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ClassesGivenViewModel {
  let classes: [CellConfiguratorType]
  let hasMore: Bool
  
  init(classes: [ClassesGivenCellViewModel], hasMore: Bool) {
    self.hasMore = hasMore
    self.classes = classes.map {
      CellConfigurator<ClassesGivenCell>(viewModel: $0)
    }
  }
  
  init(classes: [NSDate], hasMore: Bool) {
    let cellViewModels = classes.map {
      ClassesGivenCellViewModel(date: $0)
    }
    self.init(classes: cellViewModels, hasMore: hasMore)
  }
}

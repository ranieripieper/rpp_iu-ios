//
//  ProfessionalsViewModel.swift
//  Iu
//
//  Created by Gilson Gil on 4/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ProfessionalsViewModel {
  let professionals: [Professional]
  var professionalItems: [CellConfiguratorType]
  
  init(professionals: [Professional]) {
    self.professionals = professionals
    self.professionalItems = professionals.map {
      CellConfigurator<ProfessionalsCell>(viewModel: ProfessionalsCellViewModel(name: $0.name, prefix: $0.type.prefix()))
    }
  }
}

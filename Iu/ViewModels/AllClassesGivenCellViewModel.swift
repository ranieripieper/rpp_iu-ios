//
//  AllClassesGivenCellViewModel.swift
//  iu.Professor
//
//  Created by Gilson Gil on 3/30/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct AllClassesGivenCellViewModel {
  let month: String
  let dates: [CellConfiguratorType]
}

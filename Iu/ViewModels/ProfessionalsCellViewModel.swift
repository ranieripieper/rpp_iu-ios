//
//  ProfessionalsCellViewModel.swift
//  Iu
//
//  Created by Gilson Gil on 4/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ProfessionalsCellViewModel {
  let name: String
  let prefix: String
}

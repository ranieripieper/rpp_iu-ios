//
//  Professional.swift
//  Iu
//
//  Created by Gilson Gil on 4/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

enum ProfessionalType {
  case Professor, Doctor
  
  func prefix() -> String {
    switch self {
    case .Professor:
      return "Prof. "
    case .Doctor:
      return "Dr. "
    }
  }
}

enum PaymentType {
  case Before, After
}

struct Professional {
  let name: String
  let objectId: String
  let studentName: String?
  let recentClasses: [PFObject]
  let classesCount: Int
  let type = ProfessionalType.Professor
  let payment: PaymentType?
  let significantDate: NSDate?
  
  init?(json: [String: AnyObject]) {
    guard let name = json["name"] as? String, let objectId = json["objectId"] as? String else {
      return nil
    }
    self.name = name
    self.objectId = objectId
    let summary = json["summary"]
    recentClasses = summary?["recent_classes"] as? [PFObject] ?? []
    classesCount = summary?["qt_classes"] as? Int ?? 0
    payment = (summary?["type"] as? Int) == 1 ? .Before : .After
    significantDate = summary?["date"] as? NSDate ?? NSDate(timeIntervalSince1970: 0)
    studentName = summary?["student_name"] as? String ?? ""
  }
  
  init?(name: String?, objectId: String?) {
    guard let name = name, objectId = objectId else {
      return nil
    }
    self.name = name
    self.objectId = objectId
    recentClasses = []
    classesCount = 0
    payment = nil
    significantDate = nil
    studentName = nil
  }
  
  init?(professional: Professional, summary: [String: AnyObject]) {
    self.name = professional.name
    self.objectId = professional.objectId
    recentClasses = summary["recent_classes"] as? [PFObject] ?? []
    classesCount = summary["qt_classes"] as? Int ?? 0
    payment = (summary["type"] as? Int) == 1 ? .Before : .After
    significantDate = summary["date"] as? NSDate ?? NSDate(timeIntervalSince1970: 0)
    studentName = summary["student_name"] as? String ?? ""
  }
}

//
//  IuUITests.swift
//  IuUITests
//
//  Created by Gilson Gil on 2/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import XCTest

class IuUITests: XCTestCase {
  override func setUp() {
    super.setUp()
    
    let app = XCUIApplication()
    setupSnapshot(app)
    app.launch()
  }
  
  func testTakeScreenshots() {
    let app = XCUIApplication()
    
    snapshot("01 Login")
    
    let qualSeuNumeroDeTelefoneTextField = app.textFields["Qual seu número de telefone?"]
    qualSeuNumeroDeTelefoneTextField.typeText("11987654321")
    
    snapshot("02 Login 2")
    
    let deleteKey = app.keys["Delete"]
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    deleteKey.tap()
    qualSeuNumeroDeTelefoneTextField.typeText("11996483296")
    
    app.buttons["OK"].tap()
    XCUIApplication().scrollViews.otherElements.containingType(.StaticText, identifier:"Olá, Gil, você tem").element.swipeUp()
    XCUIApplication().scrollViews.otherElements.containingType(.StaticText, identifier:"Olá, Gil, você tem").element.swipeUp()
    
    snapshot("03 Aulas")
  }
}
